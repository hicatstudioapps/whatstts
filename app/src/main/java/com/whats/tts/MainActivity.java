package com.whats.tts;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.TextView.OnEditorActionListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import java.util.ArrayList;

import badabing.lib.ServerUtilities;
import badabing.lib.apprater.AppRater;

public class MainActivity extends AppCompatActivity {
   String adID;
   Builder builder;
   CheckBox cbox;
   Editor editor;
   EditText et;
   Typeface font;
   String lang;
   MainActivity.MyRecognitionListener listener;
   private InterstitialAd mInterstitialAd;
   ProgressBar pbar;
   SharedPreferences settings;
   Spinner spinner1;
   SpeechRecognizer sr;
   TextView title;
   Toolbar toolbar;
   private AdView adView;

   public void goToNextLevel() {
   }

   protected void onCreate(Bundle var1) {
      super.onCreate(var1);
            this.setContentView(R.layout.activity_main);
      toolbar=(Toolbar)findViewById(com.whats.tts.R.id.tabanim_toolbar);

      setSupportActionBar(toolbar);
      ServerUtilities.registerWithGCM(this);
      this.et = (EditText)this.findViewById(com.whats.tts.R.id.editText1);
      AdRequest var13;
      try {
         this.adView = (AdView)this.findViewById(com.whats.tts.R.id.adView);
         var13 = (new AdRequest.Builder()).build();
         this.adView.loadAd(var13);
         this.adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
               super.onAdLoaded();
               adView.setVisibility(View.VISIBLE);
            }
         });
      } catch (Exception var8) {
         ;
      }
      this.sr = SpeechRecognizer.createSpeechRecognizer(this);
      this.listener = new MainActivity.MyRecognitionListener();
      this.sr.setRecognitionListener(this.listener);
      this.spinner1 = (Spinner)this.findViewById(com.whats.tts.R.id.spinner1);
      this.spinner1.setOnItemSelectedListener(new MainActivity.CustomOnItemSelectedListener());
      this.builder = new Builder(this);
     // this.pbar = (ProgressBar)this.findViewById(com.whats.tts.R.id.progressBar1);
//      this.pbar.setMax(20);
      this.cbox = (CheckBox)this.findViewById(com.whats.tts.R.id.checkBox1);
      this.title = (TextView)this.findViewById(com.whats.tts.R.id.title);
//      this.font = Typeface.createFromAsset(this.getAssets(), "MonaKo.ttf");
      //this.title.setTypeface(this.font);
      this.et.setImeOptions(6);
      this.et.setOnEditorActionListener(new OnEditorActionListener() {
         public boolean onEditorAction(TextView var1, int var2, KeyEvent var3) {
            if(var2 == 6) {
               MainActivity.this.et.clearFocus();
            }
            return false;
         }
      });
      this.settings = this.getSharedPreferences("com.whats.sst", 0);
      this.editor = this.settings.edit();
      this.spinner1.setSelection(this.settings.getInt("Pref_lang", 0));
   }

   public boolean onCreateOptionsMenu(Menu var1) {
      getMenuInflater().inflate(com.whats.tts.R.menu.main, var1);
      return true;
   }

   protected void onDestroy() {
      super.onDestroy();
   }

   public boolean onOptionsItemSelected(MenuItem var1) {
      boolean var2;
      switch (var1.getItemId()){
         case com.whats.tts.R.id.close:{
            finish();
            SpotTheCatApp.showInterstitial();
            break;
         }
         case R.id.shre:
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            String text = String.format(getString(R.string.share_app_default_text), getString(R.string.app_name), "https://play.google.com/store/apps/details?id="+ getPackageName());
            sendIntent.putExtra(Intent.EXTRA_TEXT, text);
            sendIntent.setType("text/plain");
            startActivity(sendIntent);


            break;
         case R.id.more:
         startActivity(new Intent(this,OutrasAppsActivity.class));
            break;
         case R.id.rate:
            AppRater.rateNow(this);
            break;
      }
     return true;

   }

   protected void onPause() {
      super.onPause();
   }

   protected void onRestart() {
      super.onRestart();
   }

   protected void onResume() {
      super.onResume();
   }

   protected void onStart() {
      super.onStart();
   }

   protected void onStop() {
      super.onStop();
   }

   public void reset(View var1) {
      this.et.setText("");
   }

   @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);
      switch (requestCode) {
         case 555: {
            if (resultCode == RESULT_OK && null != data) {
               ArrayList<String> result = data
                       .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
               et.setText(result.get(0));
            }
            break;
         }
      }
   }

   public void send(View var1) {
      Intent var2;
      if(this.cbox.isChecked()) {
         var2 = new Intent("android.intent.action.SEND");
         var2.setType("text/plain");
         var2.setPackage("com.whatsapp");
         var2.putExtra("android.intent.extra.TEXT", this.et.getText().toString());
         if(getPackageManager().getLaunchIntentForPackage("com.whatsapp") != null) {
            this.startActivity(var2);
         } else {
            Toast.makeText(this, "WhatsApp isn\'t installed", Toast.LENGTH_SHORT).show();
         }
      } else {
         var2 = new Intent("android.intent.action.SENDTO");
         var2.setData(Uri.parse("smsto:"));
         var2.putExtra("sms_body", this.et.getText().toString());
          this.startActivity(var2);
      }

   }

   public void startListen(View var1) {
      Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
      intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
              RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
      intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, this.lang);
//      intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"hghhggh");
      startActivityForResult(intent, 555);
//      Intent var2 = new Intent("android.speech.action.RECOGNIZE_SPEECH");
//      var2.putExtra("android.speech.extra.LANGUAGE", this.lang);
//      this.sr.startListening(var2);
   }

   class CustomOnItemSelectedListener implements OnItemSelectedListener {

      public void onItemSelected(AdapterView var1, View var2, int var3, long var4) {
         MainActivity.this.editor.putInt("Pref_lang", var3);
         MainActivity.this.editor.commit();
         switch(var3) {
         case 0:
            MainActivity.this.lang = "ar-SA";
            break;
         case 1:
            MainActivity.this.lang = "en-US";
            break;
         case 2:
            MainActivity.this.lang = "en-UK";
            break;
         case 3:
            MainActivity.this.lang = "it-IT";
            break;
         case 4:
            MainActivity.this.lang = "fr-FR";
            break;
         case 5:
            MainActivity.this.lang = "es-ES";
            break;
         case 6:
            MainActivity.this.lang = "ja-JP";
            break;
         case 7:
            MainActivity.this.lang = "de-DE";
            break;
         case 8:
            MainActivity.this.lang = "ru-RU";
            break;
         case 9:
            MainActivity.this.lang = "hi-IN";
            break;
         case 10:
            MainActivity.this.lang = "id-ID";
            break;
         default:
            MainActivity.this.lang = "ar-SA";
         }

      }

      public void onNothingSelected(AdapterView var1) {
      }
   }

   class MyRecognitionListener implements RecognitionListener {
      public void onBeginningOfSpeech() {
      }

      public void onBufferReceived(byte[] var1) {
          Toast.makeText(getApplicationContext(),"Recived",Toast.LENGTH_SHORT).show();
      }

      public void onEndOfSpeech() {
          Toast.makeText(getApplicationContext(),"FIN",Toast.LENGTH_SHORT).show();
      }

      public void onError(int var1) {
       //
          Toast.makeText(getApplicationContext(),"Error:"+var1,Toast.LENGTH_SHORT).show();
      }

      public void onEvent(int var1, Bundle var2) {

      }

      public void onPartialResults(Bundle var1) {

      }

      public void onReadyForSpeech(Bundle var1) {

      }

      public void onResults(Bundle var1) {
         final ArrayList var3 = var1.getStringArrayList("results_recognition");
         CharSequence[] var2 = (CharSequence[])var3.toArray(new CharSequence[var3.size()]);
         MainActivity.this.builder.setTitle("Choose one:");
         MainActivity.this.builder.setItems(var2, new OnClickListener() {
            public void onClick(DialogInterface var1, int var2) {
               MainActivity.this.et.append(((String)var3.get(var2)).toString() + ". ");
            }
         });
         MainActivity.this.builder.show();
      }

      public void onRmsChanged(float var1) {
         MainActivity.this.pbar.setProgress((int)var1);
      }
   }
}
